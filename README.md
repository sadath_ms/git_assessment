# git_assessment

**Create a new branch off master:**
    - git branch -b <branch_name>

git fetch 

git branch
  - master
  - role_features-sadath
  - branch-2

**Create a new branch off exisiting branch , instead of master**

   - git branch -b <new_branch> <exisiting_branch>

**Merge Conflicts**

   - created a new branch and edited "views.py" file and added today_date() function in the line of 36, 37 and 38 then commit and pushed
   - checkout master branch and edited "views.py" file and added today_date_time() function in the line of 36,37 and 38 then commit and pushed
   - create a merge request
   - rebase new branch with master   **git rebase master**
   - fix conflicts and stage the file **git add views.py**
   - continue rebasing **git rebase --continue**
   - push 
