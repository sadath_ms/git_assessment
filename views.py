import datetime as dtt


def get_user_details():
    response = {
        "user_id": 1232,
        "username": f"user name is {'sadat'}"
    }
    return response


def role_details():
    response = [
        {
            "id": 1,
            "role_name": "Teacher"
        },
        {
            "id": 12,
            "role_name": "Principal"
        },
    ]

    return response

def hello_python():
    data = "hello python"
    return data


def hello():
    data = "HELLO PYTHON"
    return data


def today_date():
    today = f"today date is {dtt.datetime.date()}"
    return today


def today_date_time_is():
    date_time = f"today date and time is {dtt.datetime.today()}"
    return date_time

